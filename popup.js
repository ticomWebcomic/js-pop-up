class PopUp {
  constructor(button, popup, close) {
    this.boton = button;
    this.popup = popup;
    this.cerrar = close;
  }

  mostrarPopUp() {
    this.popup.style.display = "block";
  }

  cerrarPopUp() {
    this.popup.style.display = "none";
  }
}

export { PopUp };
