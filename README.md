# Pop-up con Vanilla JS

Un ejemplo de como hacer un pop-up usando solo JS

## Your Project

### ← README.md

That's this file, where you can tell people what your cool website does and how you built it.

### ← index.html

El ejemplo

### ← style.css

Estilo para el pop-up

### ← script.js

Código para hacer funcionar el Pop-up

### ← assets

Drag in `assets`, like images or music, to add them to your project

## Made by [Glitch](https://glitch.com/)

\ ゜ o ゜)ノ
