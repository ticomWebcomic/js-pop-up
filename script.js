import { PopUp } from "/popup.js";

const button = document.querySelector("button");
const popup = document.querySelector(".popup-wrapper");
const close = document.querySelector(".popup-close");
const accionPopUp = new PopUp(button, popup, close);

button.addEventListener("click", () => {
  accionPopUp.mostrarPopUp();
});

close.addEventListener("click", () => {
  accionPopUp.cerrarPopUp();
});

popup.addEventListener("click", e => {
  if (e.target.className === "popup-wrapper") {
    accionPopUp.cerrarPopUp();
  }
});
